# Create your views here.
from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth import authenticate, login, logout
from django.http import Http404
from .models import Book


def search(request):
    
    errors = []
    if 'q' in request.GET:
        q = request.GET['q']
        if not q:
            errors.append("Please submit a search term.")

        elif len(q) > 20:
            errors.append("Please enter max of 20 characters")

        else:
            books = Book.objects.filter(title__icontains=q)
            return render(
                request,
                'search_results.html',
                {'books': books, 'query': q},
            )

    return render(request, 'search_form.html', {'errors': errors})

def register(request):
    
    errors = []
    if 'q' in request.GET:
        q = request.GET['q']
        if not q:
            errors.append("Please submit a search term.")

        elif len(q) > 20:
            errors.append("Please enter max of 20 characters")

        else:
            books = Book.objects.filter(title__icontains=q)
            return render(
                request,
                'search_results.html',
                {'books': books, 'query': q},
            )

    return render(request, 'registration.html', {'errors': errors})

def login(request):
    
    errors = []
    if 'q' in request.GET:
        q = request.GET['q']
        if not q:
            errors.append("Please submit a search term.")

        elif len(q) > 20:
            errors.append("Please enter max of 20 characters")

        else:
            books = Book.objects.filter(title__icontains=q)
            return render(
                request,
                'search_results.html',
                {'books': books, 'query': q},
            )

    return render(request, 'registration.html', {'errors': errors})



def index(request):
    books = Book.objects.all()
    return render(request, 'books/books.html', {'books': books})


def detail(request, pk):
    try:
        book = Book.objects.get(pk=pk)
    except Book.DoesNotExist:
        raise Http404()

    return render(request, 'books/book.html', {'book': book})