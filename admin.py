from django.contrib import admin

# Register your models here.

from .models import Author, Book, Publisher

class AuthorAdmin(admin.ModelAdmin):
    fields = ['email', 'first_name', 'last_name']
    list_display = ['last_name', 'first_name', 'email']
    list_filter = ['first_name', 'last_name']

class BookAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Title', {'fields': ['title', 'authors', 'publisher']}),
        ('Date information', {
            'fields': ['publication_date'],
            'classes': ['collapse']
        }),
    ]
    list_display = ['title', 'publisher', 'publication_date', 'was_published_recently']
    list_filter = ['publication_date']


class BookInline(admin.StackedInline):
    model = Book
    extra = 3

class PublisherAdmin(admin.ModelAdmin):
    fields = ['name', 'city', 'country', 'website']
    inlines = [BookInline]
    
    list_display = ['name', 'city', 'country', 'website']
    search_fields = ['name', 'city', 'country', 'website']


admin.site.register(Author, AuthorAdmin)
admin.site.register(Book, BookAdmin)
admin.site.register(Publisher, PublisherAdmin)