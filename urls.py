from django.conf.urls import patterns, url

from .views import search,login,register, index, detail


urlpatterns = patterns('',
    url(r'^$', index, name="index"),
    url(r'^(\d+)/$', detail, name="detail"),
    url(r'^search/$', search),
    url(r'^login/$', login),
    url(r'^registration/$', register),
    
)

