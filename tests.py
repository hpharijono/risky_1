# Create your tests here.
"""
begin with test
"""
import datetime

from django.utils import timezone
from django.test import TestCase
from django.core.urlresolvers import reverse

from .models import Book, Publisher, Author



class BookMethodTests(TestCase):
    def test_was_published_recently_with_future_book(self):
        """
        was_published_recently() should return False for books
        whose publication_date is in the future.
        """
        future_date = timezone.now().date() + datetime.timedelta(days=30)
        future_book = Book(publication_date=future_date)
        self.assertEqual(future_book.was_published_recently(), False)

    def test_was_published_recently_with_old_book(self):
        pub_date = timezone.now().date() - datetime.timedelta(days=30)
        old_book = Book(publication_date=pub_date)
        self.assertEqual(old_book.was_published_recently, False)


class IndexViewTests(TestCase):
    def setUp(self):
        self.publisher = Publisher.objects.create(
            name='test',
            address='test',
            city='test',
            state_province='test',
            country='test',
            website='test',
        )
        self.author = Author.objects.create(
            first_name='test',
            last_name='test',
            email='test@gmail.com')
        book = Book.objects.create(
            title='test',
            publisher=self.publisher,
            publication_date=timezone.now())
        self.book = book
        self.book.authors.add(self.author)

    def test_index_view_with_books(self):
        """Books should be displayed if some books exist."""
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(
            response.context['books'],
            [repr(r) for r in Book.objects.all()],
        )

    def test_index_view_with_no_books(self):
        """Display appropriate message if no books exist."""
        Book.objects.all().delete()
        response = self.client.get(reverse('index'))
        self.assertContains(response, "No books are available.")
        self.assertQuerysetEqual(
            response.context['books'],
            [],
        )